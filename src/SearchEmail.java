

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

/**
 * Servlet implementation class SearchEmail
 */
@WebServlet("/search_email")
public class SearchEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchEmail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String data = request.getParameter("data");
		System.out.println("Got task search request: " + data);
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
		Root<Employee> c = cq.from(Employee.class);
		String pattern = "%" + data + "%";
		System.out.println("Searching for pattern: " + pattern);
		cq.select(c).where(cb.like(cb.lower(c.get("email")), pattern));
		
		TypedQuery<Employee> query = entityManager.createQuery(cq);
		List<Employee> tasks = query.getResultList();
		List<SearchEntry> entries = new ArrayList();
		
		System.out.println("Found " + tasks.size() + " number of entries.");
		
		for (Employee e : tasks) {
			entries.add(new SearchEntry(e.getEmail(), Long.toString(e.getId())));
		}
		
		JSONObject result = new JSONObject();
		result.put("suggestions", entries);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		response.getWriter().print(result.toJSONString());
		response.getWriter().flush();
		
		System.out.println("Returning search result: " + result.toJSONString());
		
		entityManager.close();
		entityManagerFactory.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
