
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class GetTaskServlet
 */
@WebServlet("/get_task")
public class GetTaskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetTaskServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String taskIdStr = request.getParameter("task");
		if (taskIdStr == null) {
			RequestDispatcher view = request.getRequestDispatcher("TaskList.html");
			view.forward(request, response);
		} else if (taskIdStr.equals("ALL")) {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
			EntityManager entityManager = entityManagerFactory.createEntityManager();

			TypedQuery<Task> query = entityManager.createQuery("SELECT t FROM Task t", Task.class);
			List<Task> tasks = query.getResultList();
			System.out.println("Sending all " + tasks.size() + " tasks.");

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("success", true);
			jsonResponse.put("tasks", tasks);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(jsonResponse.toJSONString());
			response.getWriter().flush();

			System.out.println("Tasks: " + jsonResponse.toJSONString());

			entityManager.close();
			entityManagerFactory.close();
		} else {
			String data = request.getParameter("data");
			if (data == null || data.equals("false")) {
				System.out.println("Returning TaskEdit page for taskid" + taskIdStr);
				RequestDispatcher view = request.getRequestDispatcher("TaskEdit.html");
				view.forward(request, response);
			} else {
				EntityManagerFactory entityManagerFactory = Persistence
						.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
				EntityManager entityManager = entityManagerFactory.createEntityManager();
				
				System.out.println("Returning data for taskid " + taskIdStr);
				
				long taskID = Long.parseLong(taskIdStr);
				
				Task task = entityManager.find(Task.class, taskID);
				
				if (task == null) {
					System.out.println("Couldn't find task with id " + taskIdStr);
					returnError(response, "Couldn't find task with ID " + taskIdStr);
					entityManager.close();
					entityManagerFactory.close();
					return;
				}
				
				JSONObject json = new JSONObject();
				json.put("success", true);
				json.put("task", task);
				
				Query q1 = entityManager.createQuery("SELECT p FROM Project AS p");
				List<Project> projects = q1.getResultList();
				
				json.put("projects", projects);
				
				System.out.println("Returning result: " + json.toJSONString());
				
				response.setContentType("application/json");
				response.setCharacterEncoding("utf-8");		
				response.getWriter().print(json.toJSONString());
				response.getWriter().flush();
				
				entityManager.close();
				entityManagerFactory.close();
			}
		}
	}

	private void returnError(HttpServletResponse response, String error) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("reason", error);
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String taskIdStr = request.getParameter("task");
		String summary = request.getParameter("summary");
		String estimateStr = request.getParameter("estimate");
		String projectStr = request.getParameter("project");
		
		if (taskIdStr == null || taskIdStr.isEmpty()) {
			returnError(response, "Task ID is empty.");
			return;
		}
		
		if (summary == null || summary.isEmpty()) {
			returnError(response, "Summary is empty.");
			return;
		}
		
		if (estimateStr == null || estimateStr.isEmpty()) {
			returnError(response, "Estimate is empty.");
			return;
		}
		
		if (projectStr == null || projectStr.isEmpty()) {
			returnError(response, "Project is empty.");
			return;
		}
		
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		TypedQuery<Project> tq = entityManager.createQuery("SELECT p FROM Project AS p WHERE p.name=\"" + projectStr + "\"", Project.class);
		List<Project> projects = tq.getResultList();
		
		if (projects.size() != 1) {
			returnError(response, "Problem with DB. Multiple projects with same name.");
			entityManager.close();
			entityManagerFactory.close();
			return;
		}
		
		Project project = projects.get(0);
		
		if (project == null) {
			returnError(response, "Couldn't fetch project: " + projectStr);
			entityManager.close();
			entityManagerFactory.close();
			return;
		}
		
		Date lastUpdate = new Date();
		
		Task task = entityManager.find(Task.class, Long.parseLong(taskIdStr));
		
		if (task == null) {
			returnError(response, "Couldn't find task with given ID: " + taskIdStr);
			entityManager.close();
			entityManagerFactory.close();
			return;
		}
		
		Project oldProject = task.getProject();
		
		task.setEstimate(Integer.parseInt(estimateStr));
		task.setSummary(summary);
		task.setProject(project);
		entityManager.getTransaction().begin();
		task.getProject().setLastUpdate(lastUpdate);
		entityManager.persist(task);
		
		System.out.println("Project ID: " + project.getId());
		System.out.println("Task project ID: " + task.getProject().getId());
		
		if (project.getId() != oldProject.getId()) {
			System.out.println("Persisting project also!");
			oldProject.removeTask(task.getId());
			oldProject.setLastUpdate(lastUpdate);
			project.addTask(task);
			entityManager.persist(project);
			entityManager.persist(oldProject);
		}
		
		entityManager.getTransaction().commit();
		
		JSONObject json = new JSONObject();
		json.put("success", true);
		System.out.println("Edited task: " + task.toString());
		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");		
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
		
		entityManager.close();
		entityManagerFactory.close();
	}

}
