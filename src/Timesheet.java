
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class Timesheet
 */
@WebServlet("/work_logs")
public class Timesheet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Timesheet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String employeeIdStr = request.getParameter("email");
		if (employeeIdStr == null) {
			System.out.println("Returning empty timesheet page.");
			RequestDispatcher view = request.getRequestDispatcher("WorkLogs.html");
			view.forward(request, response);
			return;
		}
		
		if (employeeIdStr.isEmpty()) {
			returnError(response, "Email is empty!");
			return;
		}
		
		String startDateStr = request.getParameter("start_date");
		String endDateStr = request.getParameter("end_date");
		System.out.println("Returning timesheet: " + employeeIdStr + " s: " + startDateStr + " e: " + endDateStr);
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			Date startDate = dateFormat.parse(startDateStr);
			Date endDate = dateFormat.parse(endDateStr);
			
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<WorkLog> cq = cb.createQuery(WorkLog.class);
			Root<WorkLog> c = cq.from(WorkLog.class);
			Join<WorkLog, Employee> join = c.join("employee", JoinType.INNER);
			
			long employeeId = Long.parseLong(employeeIdStr);
			
			cq.select(c).where(cb.and(cb.between(c.get("date"), startDate, endDate), cb.equal(join.get("id"), employeeId)));
			cq.orderBy(cb.asc(c.get("date")));
			
			TypedQuery<WorkLog> query = entityManager.createQuery(cq);
			List<WorkLog> workLogs = query.getResultList();
			
			JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("success", true);
            jsonResponse.put("worklogs", workLogs);
            
            System.out.println("Returning timesheet: " + jsonResponse.toJSONString());
            
            response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(jsonResponse.toJSONString());
			response.getWriter().flush();
			
			entityManager.close();
			entityManagerFactory.close();
			
		} catch(ParseException err) {
			returnError(response, "Error while parsing date: " + err.getMessage());
			return;
		}
	}
	
	private void returnError(HttpServletResponse response, String error) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("reason", error);
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
