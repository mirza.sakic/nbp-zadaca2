import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

@Entity
public class Task implements Serializable, JSONAware {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long estimate;
	private String summary;
	
	@ManyToOne (cascade = CascadeType.PERSIST)
	@Column(nullable = false)
	private Project project;
	
	@OneToMany (cascade = CascadeType.REMOVE)
	private Set<WorkLog> workLogs;
	
	public Task(long estimate, String summary, Project project) {
		super();
		this.estimate = estimate;
		this.summary = summary;
		this.project = project;	
		this.workLogs = new HashSet<WorkLog>();
	}
	
	@Override
	public String toString() {
		return "Task [id=" + id + ", estimate=" + estimate + ", summary=" + summary + ", | " + project.toString() + "]";
	}
	
	public void addWorkLog(WorkLog worklog) {
		workLogs.add(worklog);
	}
	
	public void removeWorkLog(WorkLog worklog) {
		workLogs.remove(worklog);
	}
	
	public void removeWorkLog(long id) {
		WorkLog res = null;
		for (WorkLog w : workLogs) {
			if (w.getId() == id) {
				res = w;
				break;
			}
		}
		if (res != null) {
			workLogs.remove(res);
		}
	}
	
	public long getEstimate() {
		return estimate;
	}
	public void setEstimate(long estimate) {
		this.estimate = estimate;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public long getId() {
		return id;
	}
	
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
		
	}
	@Override
	public String toJSONString() {
		JSONObject res = new JSONObject();
		res.put("id", id);
		res.put("summary", summary);
		res.put("estimate", estimate);
		res.put("project", project);
		return res.toJSONString();
	}
}
