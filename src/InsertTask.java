
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class InsertTask
 */
@WebServlet("/insert_task")
public class InsertTask extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InsertTask() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String data = request.getParameter("data");
		if (data == null) {
			RequestDispatcher view = request.getRequestDispatcher("InsertTask.html");
			view.forward(request, response);
		} else {
			System.out.println("Returning project data: " + data);
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			Query q1 = entityManager.createQuery("SELECT p FROM Project AS p");
			List<Project> results = q1.getResultList();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("success", true);
			jsonResponse.put("projects", results);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			System.out.println("Returning data: " + jsonResponse.toJSONString());
			response.getWriter().print(jsonResponse.toJSONString());
			response.getWriter().flush();
			entityManager.close();
			entityManagerFactory.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		String project = request.getParameter("project");
		String summary = request.getParameter("summary");
		int estimate = Integer.parseInt(request.getParameter("estimate"));

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Project> cq = cb.createQuery(Project.class);
		Root<Project> c = cq.from(Project.class);
		cq.select(c).where(cb.equal(c.get("name"), project));

		TypedQuery<Project> query = entityManager.createQuery(cq);
		List<Project> projects = query.getResultList();

		if (projects.size() == 1) {
			Project p = projects.get(0);
			System.out.println("Found project: " + p.toString());
			Task t = new Task(estimate, summary, p);
			p.addTask(t);
			t.getProject().setLastUpdate(new Date());
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(t);
				entityManager.getTransaction().commit();
				
				JSONObject jsonResponse = new JSONObject();
				jsonResponse.put("success", true);
				response.setContentType("application/json");
				response.setCharacterEncoding("utf-8");
				response.getWriter().print(jsonResponse.toJSONString());
				response.getWriter().flush();
			} catch (RollbackException e) {
				System.out.println("Caught RollbackException: " + e.getMessage());
				returnError(response, "Rollback exception: " + e.getMessage());
			} catch (EntityExistsException e) {
				System.out.println("Caught EntityExistsException: " + e.getMessage());
				returnError(response, "EntityExistsException: " + e.getMessage());
			} catch (Exception e) {
				System.out.println("Caught Exception: " + e.getMessage());
				returnError(response, "Exception: " + e.getMessage());
			}
		} else {
			System.out.println("Error with number of fetched projects: " + projects.size());
			returnError(response, "Error with number of projects.");
		}

		entityManager.close();
		entityManagerFactory.close();
	}

	private void returnError(HttpServletResponse response, String error) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("reason", error);
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
	}

}
