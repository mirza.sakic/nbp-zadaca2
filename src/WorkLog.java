import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

@Entity
public class WorkLog implements Serializable, JSONAware {
	

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Task task;
	
	@Column(nullable = false)
	private Employee employee;
	
	@Column(nullable = false)
	private int hours;
	
	@Column(nullable = false)
	private Date date;
	
	@Override
	public String toJSONString() {
		// TODO Auto-generated method stub
		JSONObject res = new JSONObject();
		res.put("id", id);
		res.put("summary", task.getSummary()); 
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
		String strDate = dateFormat.format(date);  
		res.put("date", strDate);
		res.put("hours", hours);
		return res.toJSONString();
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getId() {
		return id;
	}

	public WorkLog(Task task, Employee employee, int hours, Date date) {
		super();
		this.task = task;
		this.employee = employee;
		this.hours = hours;
		this.date = date;
	}

	@Override
	public String toString() {
		return "WorkLog [id=" + id + ", task=" + task + ", employee=" + employee + ", hours=" + hours + ", date=" + date
				+ "]";
	}

}
