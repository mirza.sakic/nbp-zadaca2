

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class ServerListener
 *
 */
@WebListener
public class ServerListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public ServerListener() {
    	
        
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
         // TODO Auto-generated method stub
    	
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
         // TODO Auto-generated method stub
    	EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        TypedQuery<Employee> query = entityManager.createQuery("SELECT e FROM Employee e", Employee.class);
//        List<Employee> res = query.getResultList();
//        System.out.println(res.size());
//        Position p = new Position("Intern", 14.5);
//        Employee e = new Employee("Mirza", "Sakic", p);
//        entityManager.getTransaction().begin();
//        entityManager.persist(p);
//        entityManager.persist(e);
//        
//        entityManager.getTransaction().commit();
        
        TypedQuery<Project> tq = entityManager.createQuery("SELECT p FROM Project AS p", Project.class);
		List<Project> projects = tq.getResultList();
		
		for (Project p : projects) {
			for (Task t : p.getTasks()) {
				System.out.println("P: " + p.getName() + " | " + t.toString());
			}
		}
		
		//Project p = projects.get(0);
		
		//if (p == null) System.out.println("Koji k?");
		
		//entityManager.getTransaction().begin();
		//entityManager.remove(p);

		//entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }
	
}
