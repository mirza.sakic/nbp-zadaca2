import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

class SearchEntry implements JSONAware {
	private String label;
	private String value;
	public SearchEntry(String label, String value) {
		super();
		this.label = label;
		this.value = value;
	}
	@Override
	public String toJSONString() {
		JSONObject res = new JSONObject();
		res.put("label", label);
		res.put("value", value);
		return res.toJSONString();
	}	
}
