import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class PartnerEmployee extends Employee implements Serializable {
	
	private double salaryPerHour;
	private int yearsInTheCompany;

	public double getHourlyBonus() {
		if (yearsInTheCompany < 5) return 5.;
		if (yearsInTheCompany < 10) return 10.;
		if (yearsInTheCompany < 15) return 20.;
		return 30.;
	}
	
	public PartnerEmployee(String firstName, String lastName, String email, int age, double salaryPerHour,
			int yearsInTheCompany) {
		super(firstName, lastName, email, age);
		this.salaryPerHour = salaryPerHour;
		this.yearsInTheCompany = yearsInTheCompany;
	}

	@Override
	public double getSalaryPerHour() {
		// TODO Auto-generated method stub
		return salaryPerHour + getHourlyBonus();
	}

	@Override
	public String toString() {
		return "PartnerEmployee [salaryPerHour=" + salaryPerHour + ", yearsInTheCompany=" + yearsInTheCompany
				+ " | " + super.toString() + "]";
	}

	public int getYearsInTheCompany() {
		return yearsInTheCompany;
	}

	public void setYearsInTheCompany(int yearsInTheCompany) {
		this.yearsInTheCompany = yearsInTheCompany;
	}

}
