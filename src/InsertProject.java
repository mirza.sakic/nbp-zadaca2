
import java.io.IOException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class InsertProject
 */
@WebServlet("/insert_project")
public class InsertProject extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InsertProject() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher view = request.getRequestDispatcher("InsertProject.html");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		String name = request.getParameter("name");
		Project p = new Project(name);
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(p);
			entityManager.getTransaction().commit();

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("success", true);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(jsonResponse.toJSONString());
			response.getWriter().flush();
		} catch (RollbackException e) {
			System.out.println("Caught RollbackException: " + e.getMessage());
			returnProjectAlreadyExists(response);
		} catch (EntityExistsException e) {
			System.out.println("Caught EntityExistsException: " + e.getMessage());
			returnProjectAlreadyExists(response);
		} catch (Exception e) {
			System.out.println("Caught Exception: " + e.getMessage());
			returnProjectAlreadyExists(response);
		}

		entityManager.close();
		entityManagerFactory.close();
	}

	private void returnProjectAlreadyExists(HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("reason", "Project with provided name already exists.");
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
	}

}
