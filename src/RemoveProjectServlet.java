
import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class RemoveProjectServlet
 */
@WebServlet("/remove_project")
public class RemoveProjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RemoveProjectServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String projectsStr = request.getParameter("projects");
		if (projectsStr == null || !projectsStr.equals("ALL")) {
			RequestDispatcher view = request.getRequestDispatcher("RemoveProject.html");
			view.forward(request, response);
		} else {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
			EntityManager entityManager = entityManagerFactory.createEntityManager();

			System.out.println("Returning data for all projects ");

			TypedQuery<Project> tq = entityManager.createQuery("SELECT p FROM Project AS p", Project.class);
			List<Project> projects = tq.getResultList();

			JSONObject json = new JSONObject();
			json.put("success", true);
			json.put("projects", projects);

			System.out.println("Returning result: " + json.toJSONString());

			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(json.toJSONString());
			response.getWriter().flush();

			entityManager.close();
			entityManagerFactory.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		String projectIdStr = request.getParameter("projectid");
		if (projectIdStr == null || projectIdStr.isEmpty()) {
			returnError(response, "Project id is empty.");
			entityManager.close();
			entityManagerFactory.close();
			return;
		}
		
		Project p = entityManager.find(Project.class, Long.parseLong(projectIdStr));
		if (p == null) {
			returnError(response, "Couldn't find project with id " + projectIdStr);
			entityManager.close();
			entityManagerFactory.close();
			return;
		}
		
		
		System.out.println("Removing project: " + p.getName());
		entityManager.getTransaction().begin();
		entityManager.remove(p);
		entityManager.getTransaction().commit();
		System.out.println("Project removed.");
		
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("success", true);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		response.getWriter().print(jsonResponse.toJSONString());
		response.getWriter().flush();
		
		entityManager.close();
		entityManagerFactory.close();
	}
	
	private void returnError(HttpServletResponse response, String error) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("reason", error);
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
	}

}
