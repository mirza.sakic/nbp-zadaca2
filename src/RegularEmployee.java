import javax.persistence.Entity;

@Entity
public class RegularEmployee extends Employee {
	
	@Override
	public String toString() {
		return "RegularEmployee [salaryPerHour=" + salaryPerHour + " | " + super.toString() + "]";
	}

	private double salaryPerHour;

	public RegularEmployee(String firstName, String lastName, String email, int age, double salaryPerHour) {
		super(firstName, lastName, email, age);
		this.salaryPerHour = salaryPerHour;
	}

	@Override
	public double getSalaryPerHour() {
		return salaryPerHour;
	}

}
