

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class InsertWorkLog
 */
@WebServlet("/insert_worklog")
public class InsertWorkLog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertWorkLog() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher view = request.getRequestDispatcher("InsertWorkLog.html");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String taskIdStr = request.getParameter("task");
		if (taskIdStr == null || taskIdStr.isEmpty()) {
			returnError(response, "Couldn't find task!");
			return;
		}
		String employeeIdStr = request.getParameter("email");
		if (employeeIdStr == null || employeeIdStr.isEmpty()) {
			returnError(response, "Couldn't find Employee with that email!");
			return;
		}
		String dateStr = request.getParameter("date");
		if (dateStr == null || dateStr.isEmpty()) {
			returnError(response, "Date is empty!");
			return;
		}
		String hoursStr = request.getParameter("hours");
		if (hoursStr == null || hoursStr.isEmpty()) {
			returnError(response, "Hours logged do not exist!");
			return;
		}
		
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Task task = entityManager.find(Task.class, Long.parseLong(taskIdStr));
		Employee employee = entityManager.find(Employee.class, Long.parseLong(employeeIdStr));
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = dateFormat.parse(dateStr);
			int hours = Integer.parseInt(hoursStr);
			WorkLog w = new WorkLog(task, employee, hours, date);
			w.getTask().addWorkLog(w);
			entityManager.getTransaction().begin();
			entityManager.persist(w);
			entityManager.getTransaction().commit();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("success", true);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(jsonResponse.toJSONString());
			response.getWriter().flush();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			returnError(response, "Invalid date!");
			entityManager.close();
			entityManagerFactory.close();
			return;
		}
		entityManager.close();
		entityManagerFactory.close();
	}
	
	private void returnError(HttpServletResponse response, String error) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("reason", error);
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
	}

}
