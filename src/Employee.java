import java.io.Serializable;

import javax.jdo.annotations.Unique;
import javax.persistence.*;

@Entity
public abstract class Employee implements Serializable {
	public Employee(String firstName, String lastName, String email, int age) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.age = age;
	}

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String firstName;
	
	private String lastName;
	
	@Unique
	private String email;
	
	private int age;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public long getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return String.format("(%d, %s, %s, %s, %d)", id, firstName, lastName, email, age);
	}
	
	public abstract double getSalaryPerHour();
}
