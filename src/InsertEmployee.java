

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class InsertEmployee
 */
@WebServlet("/insert_employee")
public class InsertEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertEmployee() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String isDataRequest = request.getParameter("data");
//		if (isDataRequest == null) {
		System.out.println("Returning employee page.");
		RequestDispatcher view = request.getRequestDispatcher("InsertEmployee.html");
        view.forward(request, response);
//		} else {
//			System.out.println("Returning data: " + isDataRequest);
//			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
//	        EntityManager entityManager = entityManagerFactory.createEntityManager();
//	        Query q1 =  entityManager.createQuery("SELECT p FROM Position AS p");
//	        List<Position> results = q1.getResultList();
//	        JSONObject jsonResponse = new JSONObject();
//	        jsonResponse.put("success", true);
//	        jsonResponse.put("positions", results);
//	        response.setContentType("application/json");
//			response.setCharacterEncoding("utf-8");
//			System.out.println("Returning data: " + jsonResponse.toJSONString());
//			PrintWriter out = response.getWriter();
//			out.print(jsonResponse.toJSONString());
//			out.flush();
//			entityManager.close();
//			entityManagerFactory.close();
//		}
	}
	
	private void returnEmployeeAlreadyExists(HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject json = new JSONObject();
		json.put("success", false);
		json.put("reason", "Employee with provided email already exists.");
		response.getWriter().print(json.toJSONString());
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("/home/mirza/fet/ciklus2/nbp/zadaca2/objectdb-2.8.3/db/zadaca2.odb");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        
        System.out.println("Got Insert Employee request.");
        
        String type = request.getParameter("type");
        
        System.out.println("Inserting " + type + " type");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String email = request.getParameter("email");
        int age = Integer.parseInt(request.getParameter("age"));
        double salaryPerHour = Double.parseDouble(request.getParameter("salary_per_hour"));

        entityManager.getTransaction().begin();
        try {
        	if (type.compareTo("regular") == 0) {
        		RegularEmployee rem = new RegularEmployee(firstName, lastName, email, age, salaryPerHour);
        		System.out.println("Inserting " + rem.toString());
        		entityManager.persist(rem);
        	
        	} else {
        		int years = Integer.parseInt(request.getParameter("years"));
        		PartnerEmployee pem = new PartnerEmployee(firstName, lastName, email, age, salaryPerHour, years);
        		System.out.println("Inserting " + pem.toString());
        		entityManager.persist(pem);
        	}
        	entityManager.getTransaction().commit();
        	JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("success", true);
            response.setContentType("application/json");
    		response.setCharacterEncoding("utf-8");
    		response.getWriter().print(jsonResponse.toJSONString());
    		response.getWriter().flush();
        } catch(RollbackException e) {
			System.out.println("Caught RollbackException: " + e.getMessage());
			returnEmployeeAlreadyExists(response);
		} catch(EntityExistsException e) {
			System.out.println("Caught EntityExistsException: " + e.getMessage());
			returnEmployeeAlreadyExists(response);
		} catch(Exception e) {
			System.out.println("Caught Exception: " + e.getMessage());
			returnEmployeeAlreadyExists(response);
		}
        
        entityManager.close();
        entityManagerFactory.close();
	}

}
