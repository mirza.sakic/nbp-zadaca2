import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.jdo.annotations.Unique;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

@Entity
public class Project implements Serializable, JSONAware {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO) 
	private long id;
	public Set<Task> getTasks() {
		return tasks;
	}
	
	@Unique
	private String name;
	
	@OneToMany(cascade = CascadeType.REMOVE)
	private Set<Task> tasks;
	
	private Date lastUpdate;
	
	
	
	@Override
	public String toJSONString() {
		JSONObject json = new JSONObject();
		json.put("id", id);
		json.put("name", name);
		json.put("lastUpdate", getLastUpdateAsString());
		return json.toJSONString();
	}
	@Override
	public String toString() {  
		return "Project [id=" + id + ", name=" + name + ", lastUpdate=" + getLastUpdateAsString() + "]";
	}
	public Project(String name) {
		super();
		this.name = name;
		this.tasks = new HashSet<Task>();
		this.lastUpdate = new Date();
	}
	
	public Project(String name, Set<Task> tasks) {
		super();
		this.name = name;
		this.tasks = tasks;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void addTask(Task task) {
		tasks.add(task);
	}
	public void removeTask(Task task) {
		tasks.remove(task);
	}
	public void removeTask(long id) {
		Task res = null;
		for(Task t : tasks) {
			if (t.getId() == id) {
				res = t;
				break;
			}
		}
		if (res != null) {
			tasks.remove(res);
		}
	}
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public String getLastUpdateAsString() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return formatter.format(lastUpdate);
	}
}
